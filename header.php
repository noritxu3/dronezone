<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin-ext" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/main.css" />
</head>
<body>
	<header id="header">
		<div class="wrapper">
			<div id="logo">
				<a href="<?= home_url(); ?>" title="<?= __('Home', 'dronezone'); ?>">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/logo.png" alt="DroneZone" width="178" height="24" />
				</a>
			</div>
			<nav id="menu">
				<?php wp_nav_menu(['theme_location' => 'primary', 'start_depth' => 1]); ?>
			</nav>

			<div id="banner">
				<h1><?php the_title(); ?></h1>

				<p class="description-banner"><?php echo get_field('banner-description'); ?></p>

				<div class="buttons-banner">
					<a href="<?= get_field('url-button-1'); ?>" class="button btn-<?= get_field('design-button-1'); ?>"> <?= get_field('text-button-1'); ?>
					</a>
					<a href="<?= get_field('url-button-2'); ?>" class="button btn-<?= get_field('design-button-2'); ?>"> <?= get_field('text-button-2'); ?>
					</a>
				</div>
			</div>
		</div>
	</header>
	<main id="main">
		<div class ="primary">
			<div class="wrapper">
				<div id="drones">
						<article>
							<div class="drone">
								<div class="image-drone">
									<a href="" title="Drone 1">
										<img src="<?php bloginfo('template_url'); ?>/assets/images/drone-1.png" alt="Drone 1" width="103" height="57" />
									</a>
								</div>
								<div class="content-drones">
									<h2 class="title-drones">From air</h2>
									<span class="description">Pellentesque eget nunc sit amet urna ullamcorper fermentum et eu leo.</span>
								</div>
							</div>
						</article>
						<article>
							<div class="drone">
								<div class="image-drone">
									<a href="" title="Drone 2">
										<img src="<?php bloginfo('template_url'); ?>/assets/images/drone-2.png" alt="Drone 2" width="97" height="98" />
									</a>
								</div>
								<div class="content-drones">
									<h2 class="title-drones">Best drones</h2>
									<span class="description">Pellentesque eget nunc sit amet urna ullamcorper fermentum et eu leo.</span>
								</div>
							</div>
						</article>
						<article>
							<div class="drone">
								<div class="image-drone">
									<a href="" title="Boussole">
										<img src="<?php bloginfo('template_url'); ?>/assets/images/boussole.png" alt="Boussole" width="84" height="84" />
									</a>
								</div>
								<div class="content-drones">
									<h2 class="title-drones">Speed</h2>
									<span class="description">Pellentesque eget nunc sit amet urna ullamcorper fermentum et eu leo.</span>
								</div>
							</div>
						</article>
						<article>
							<div class="drone">
								<div class="image-drone">
									<a href="" title="Robot">
										<img src="<?php bloginfo('template_url'); ?>/assets/images/robot.png" alt="Robot" width="71" height="95" />
									</a>
								</div>
								<div class="content-drones">
									<h2 class="title-drones">Long range</h2>
									<span class="description">Pellentesque eget nunc sit amet urna ullamcorper fermentum et eu leo.</span>
								</div>
							</div>
						</article>
				</div>	
				<div id="bouton">
					<a href="" class="bouton-askforpriece">Ask for price</a>
				</div>
			</div>
	<div class="secondary">	
		<div class="wrapper">
			<div id="numeros">
				<div class="title-red">Nature from the air</div>
				<div class="petite-description">Mauris consequat libero metus, nec ultricies sem efficitur quis. Integer bibendum eget metus ac accumsan. Integer sit amet lacus egestas, semper est quis, viverra ex.
				</div>
					<article>
						<div class="article-numeros">
							<div class="numero-1">
								<a href="" title="Article-1">
									<img src="<?php bloginfo('template_url'); ?>/assets/images/numero-1.png" alt="Drone 1" width="49" height="50" />
								</a>
							</div>
							<div class="description">Pellentesque eget nunc sit amet urna ullamcorper fermentum et eu leo. Nunc vel nibh tempor, pharetra lectus congue, luctus orci.</div>
						</div>
						<div class="article-numeros">
							<div class="numero-1">
								<a href="" title="Article-1">
									<img src="<?php bloginfo('template_url'); ?>/assets/images/numero-2.png" alt="Drone 1" width="49" height="50" />
								</a>
							</div>
							<div class="description">Pellentesque eget nunc sit amet urna ullamcorper fermentum et eu leo. Nunc vel nibh tempor, pharetra lectus congue, luctus orci.</div>
						</div>
						<div class="article-numeros">
							<div class="numero-1">
								<a href="" title="Article-1">
									<img src="<?php bloginfo('template_url'); ?>/assets/images/numero-3.png" alt="Drone 1" width="49" height="50" />
								</a>
							</div>
							<div class="description">Pellentesque eget nunc sit amet urna ullamcorper fermentum et eu leo. Nunc vel nibh tempor, pharetra lectus congue, luctus orci.</div>
						</div>
						<div class="article-numeros">
							<div class="numero-1">
								<a href="" title="Article-1">
									<img src="<?php bloginfo('template_url'); ?>/assets/images/numero-4.png" alt="Drone 1" width="49" height="50" />
								</a>
							</div>
							<div class="description">Pellentesque eget nunc sit amet urna ullamcorper fermentum et eu leo. Nunc vel nibh tempor, pharetra lectus congue, luctus orci.</div>
						</div>
						<div class="article-numeros">
							<div class="numero-1">
								<a href="" title="Article-1">
									<img src="<?php bloginfo('template_url'); ?>/assets/images/numero-5.png" alt="Drone 1" width="49" height="50" />
								</a>
							</div>
							<div class="description">Pellentesque eget nunc sit amet urna ullamcorper fermentum et eu leo. Nunc vel nibh tempor, pharetra lectus congue, luctus orci.</div>
						</div>
					</article>
			</div>		
		</div>
		<div class="troisieme">	
			<div class="wrapper">
				<div id="box">
					<div class="texte">
						<div class="title-red">Nature from the air</div>
						<div class="petite-description">Mauris consequat libero metus, nec ultricies sem efficitur quis. Integer bibendum eget metus ac accumsan. Integer sit amet lacus egestas, semper est quis, viverra ex.
						</div>
					</div>
					<div class="bouton-article">Ask for a price
					</div>
			</div>
		</div>
	</div>	
	</main>