#require 'compass/import-once/activate'

http_path       = "/"
css_dir         = "assets/css"
sass_dir        = "assets/css/scss"
images_dir      = "images"
javascripts_dir = "js"

output_style  = :compressed
line_comments = false